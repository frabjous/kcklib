<?php

session_start();
$poweruser = (
    (
        (isset($_SESSION["poweruser"]))
        &&
        ($_SESSION["poweruser"])
    ) ||
    (
        (isset($_SESSION["_ke_poweruser"]))
        &&
        ($_SESSION["_ke_poweruser"])
    ) ||
    ($_SERVER["HTTP_HOST"] == "localhost")
);
if (!$poweruser) {
    echo '[]';
    exit(1);
}

header("Access-Control-Allow-Origin: *");
exec("fc-list : family | sed 's/,.*//' | sort | uniq", $fonts);

echo json_encode($fonts);

?>