# KCKLib Readme

This is a small library of PHP and JavaScript files that contains code common to a number of my web projects. I doubt anyone else with find them particularly useful, but feel free to play around with them if you wish.

## Installation

The kckdialog elements make use of the icons found in my [Icons repository](https://bitbucket.org/frabjous/icons/), and expect them to be found in the `/icons` subfolder of the webserver.

You can install that repository with:

`git clone https://bitbucket.org/frabjous/icons.git`

And install this one with:

`git clone https://bitbucket.org/frabjous/kcklib.git`

Both should be located as immediate subfolders of the main document root of your web server. Delete or access-protect the hidden `.git` subfolders afterwards.

## Contents

### ajax.js

A javascript file that defines two commands for using AJAX calls. 

* `AJAXGetRequest(url, get_string, callback)`

  This function makes an AJAX request to *url*?*get_string* and, if successful, runs *callback* on the text returned from the server.

* `AJAXPostRequest(url, formData, callback, errorCallback [optional])`

  This function makes an AJAX POST request to *url* with a [FormData object](https://developer.mozilla.org/en-US/docs/Web/API/FormData), *formData*, runs *callback* on the response text, if successful, and *errorCallback*, if not.

### getFileList.php

A PHP script which responds to AJAX calls requesting a list of files in a given folder. Usually requires `$_SESSION["poweruser"]` to be set to true. This is called by the `kckFileChoose` function in `kckdialog.js`.

### getfonts.php

A PHP script which, if called on `localhost` or if `$_SESSION["poweruser"]` is set to true, returns the fonts installed on a Linux or similar system using fontconfig.

### hostinfo.php

A PHP library that defines three functions for information about the URL of the current page or site. Defines three functions:

* `full_host()`

   Returns the server's full hostname, including `http` or `https` as appropriate.

* `full_url()`

   Returns the current URL being acessed, including the query string.

* `full_path()`

   Returns the current URL being accessed, without the query string.

Some of the code was lifted from StackExchange answers, so technically isn't mine.

### kckdialog.css

Stylesheet for the UI elements defined in kckdialog.js.

### kckdialog.js

A very small javascript UI library for creating popup dialogs on webpages. The important functions are these:

* `kckAlert(message)`

  A popup alert box that displays the HTML in the *message*. (There are also some optional arguments you may use to change the icon displayed with it; see script for details.)

* `kckErrAlert(message)`

  This is like kckAlert, but should be used to alert the user of errors, rather than simply provide information.

* `kckYesNoBox(prompt, yescallback, nocallback)`

  This creates a dialog box that asks a yes or no question defined as *prompt*, and calls either *yescallback* or *nocallback* depending on which is clicked.

* `kckFileChoose(callback, folder, prompt)`

  Prompts the user with *prompt* to choose a file from *folder* on the server. The name of the chosen file is then passed to *callback*. Typically, `$_SESSION["poweruser"]` should be set, though there are options for more fine-grained control over which folders the user can choose from.

* `kckGenericListSelection(options, prompt, callback)`

   Gives user a menu of choices from the array *options*, prompts them to choose one with *prompt*, and calls *callback* with the option chosen.

* `kckWaitScreen()`

  Creates a spinning wheel telling the user to wait.

* `kckRemoveWait()`

  Removes the wait dialog produced by kckWaitScreen().

* `kckPopupForm(html)`

  Creates and returns a pop-form form with *html* inside it; call closeMe() on the return value to close it.

### pipe.php

A single function PHP library that defines a single command:

* `pipe_to_command($cmd, $text)`

   This function creates a process defined by `$cmd` and pipes `$text` into it. When the process finishes it returns an object that has the properties `stdin`, `stdout` and `returnvalue`, consisting of the output of the process.

### reversethis.php

A pretty useless script that returns its `s=` url parameter in reversed form. This is used for testing the operational status of the server.

### send\_as\_json.php

A PHP library primarily defining the command:

* `send_as_json($obj, $status)`

  This function encodes `$obj` as JSON and echoes it, and at the same time sets the appropriate headers for the response with a http response status of `$status`.

### stream.php

A single file PHP library that defines a single command:

* `stream($filename, $type)`

This will output (not as return value, but to the script’s standard output) the contents of a media file with name `$filename` and MIME type `$type` which defaults to "audio/mpeg", but it will stream it in chunks so as to be acceptable `src` for an HTML5 `<audio>` or `<video>` element.

### Contact Info

Kevin C. Klement ([klement@philos.umass.edu](mailto:klement@philos.umass.edu))

### License

GPL version 3.
